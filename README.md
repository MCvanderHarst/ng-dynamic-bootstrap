# This is a test project with can give inspiration for

- using an Angular app in a normal website
- using dynamic bootstrapping of Angular components
 
# First setup

Note: this setup has been made very quickly and is not entirely the right way but for test purposes it will do fine.

- Start by cloning this project
- Goto your project directory
- npm install
*(A test http server is included for running npm run serve. See below)* 
- mkdir -p dist/www/css
- Setup your normal test website in dist/www by creating an index.html and optionally add JavaScript and style files etc.
- create your Angular app the normal way, even test it the normal way

# Combining test web site with Angular

- make sure your root components (the components that will be binded to DOM objects in index.html) 
use default selectors like class name or id's
*(in the future we probably can use custom HTML elements but at the moment custom elements are only supported 
by recent versions of Mozilla and WebKit browsers)*
- npm run build && npm run package
*(This will create one complete JavaScript file call ng-lib.js.gz and copies this <project-dir>/styles.css to dist/www/css)*
- make sure the created script is called in index.html at the most bottom location as possible, preferable just before </body>
- if you have problems with the 'gz' extension, use gunzip
- npm run serve *(run a test http server)*
- browse to your website and be happy :-)
