import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-child-component',
  templateUrl: './child-component.component.html',
  styleUrls: ['./child-component.component.css']
})
export class ChildComponentComponent implements OnInit {

  showme: boolean;

  constructor() {
    this.showme = true;
  }

  ngOnInit() {
  }

  toggleMe() {
    this.showme = !this.showme;
  }

}
