import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {ButtonBarComponent} from './components/button-bar/button-bar.component';
import { ChildComponentComponent } from './components/child-component/child-component.component';

@NgModule({
  declarations: [
    AppComponent,
    ButtonBarComponent,
    ChildComponentComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  entryComponents: [
    AppComponent,
    ButtonBarComponent
  ]
})
export class AppModule {
  ngDoBootstrap(app): void {
    const appRootElement = document.querySelector('.app-root');
    if (appRootElement) {
      app.bootstrap(AppComponent);
    }

    const buttonBarElement = document.querySelector('.button-bar');
    if (buttonBarElement) {
      app.bootstrap(ButtonBarComponent);
    }
  }
}
